# Welcome
This bot welcomes your Mastodon server new users.

### Dependencies

-   **Python 3**
-   Mastodon running server
-   Mastodon server's Bot account

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python welcome.py` to check if any new users and greet them. First run will ask config parameters.

3. Use your favourite scheduling method to set `python welcome.py` to run every hour.  

17.11.2023 *New feature* detect new user's profile language. If not 'ca', warning moderators
