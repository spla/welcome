# Welcome
This bot welcomes your Mastodon server new users.

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon running server
-   Mastodon server's Bot account

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python setup.py` to get your bot's access token of your Mastodon server existing account. It will be saved to 'secrets/secrets.txt' for further use.

3. Run `python welcome.py` to check if any new users and greet them.

4. Use your favourite scheduling method to set `python welcome.py` to run every hour.
