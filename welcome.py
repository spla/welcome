from app.libraries.setup import Setup
from mastodon import Mastodon
from langdetect import detect, LangDetectException
import re
import os
import time
import sys
import os.path
import pdb

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def check_profile(notification, account_id):

    warning = False

    note = notification.account.note

    last_post = notification.account.last_status_at

    try:

        note_lang = detect(note)

        if note_lang != 'ca':

            warning = True

    except LangDetectException as error:

        pass

        if last_post != None:

            first_post = cleanhtml(mastodon.account_statuses(account_id)[int(len(mastodon.account_statuses(account_id))-1)].content)

            first_post_lang = detect(first_post)

            if first_post_lang != 'ca':

                warning = True

    return warning

def get_data(notification):

    notification_id = notification.id

    if notification.type != 'admin.sign_up':

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

        return

    account_id = notification.account.id

    username = notification.account.acct

    warning = check_profile(notification, account_id)

    if not warning:

        post_text = f'{setup.mastodon_hostname} dona la benvinguda a:\n\n'

        post_text += f'@{username}!\n\n'

        post_text += "Gràcies per acompanyar-nos!\n"

        post_text += "Ens agradaria que et presentessis una mica. Gràcies de nou!" 

        mastodon.status_post(post_text, in_reply_to_id=None, )

        print(f'Replied notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

        time.sleep(5)

    else:

        mastodon.status_post(f"@admin Alerta: possible perfil comercial de {username}", visibility='direct')

        mastodon.notifications_dismiss(notification_id)

# main

if __name__ == '__main__':

    setup = Setup()

    mastodon = Mastodon(
                access_token = setup.mastodon_app_token,
                api_base_url= setup.mastodon_hostname
                )

    bot_notifications = mastodon.notifications()

    for notif in bot_notifications:

        get_data(notif)
