from mastodon import Mastodon
import re
import os
import time
import sys
import os.path

def get_data(notification):

    notification_id = notification.id

    if notification.type != 'admin.sign_up':

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

        return

    account_id = notification.account.id

    username = notification.account.acct

    toot_text = f'{mastodon_hostname} dona la benvinguda a:\n\n'

    toot_text += f'@{username}!\n\n'

    toot_text += "Gràcies per escollir-nos!\n"

    toot_text += "Ens agradaria que et presentessis una mica. Gràcies de nou!" 

    mastodon.status_post(toot_text, in_reply_to_id=None, )

    print(f'Replied notification {notification_id}')

    mastodon.notifications_dismiss(notification_id)

def log_in():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return mastodon, mastodon_hostname

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    mastodon, mastodon_hostname = log_in()

    bot_notifications = mastodon.notifications()

    for notif in bot_notifications:

        get_data(notif)
