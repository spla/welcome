current_users_str: usuaris actuals
users_before_str: usuaris abans
users_hour_str: usuaris per hora
new_users_last_hour_str: nous usuaris darrera hora
new_users_last_day_str: nous usuaris darrer dia
new_users_last_week_str: nous usuaris darrera setmana
hourly_users_evolution_str: evolució horaria d'usuaris
in_the_last_hour_str: en la darrera hora
daily_users_evolution_str: evolució diaria d'usuaris
in_the_last_day_str: en el darrer dia
weekly_users_evolution_str: evolució setmanal d'usuaris
in_the_last_week_str: en la darrera setmana
daily_evolution_str: Evolució diaria
weekly_evolution_str: Evolució setmanal
welcomes_str: dona la benvinguda a
we_have_str: Ja som
users_str: usuaris
